defmodule HoneyPotWeb.PageHTML do
  use HoneyPotWeb, :html

  embed_templates "page_html/*"
end
