defmodule HoneyPotWeb.Layouts do
  use HoneyPotWeb, :html

  embed_templates "layouts/*"
end
