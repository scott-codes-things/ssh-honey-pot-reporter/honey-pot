defmodule HoneyPot.Keys do
  @moduledoc """
  This module generates SSH keys on boot if they dont already exist
  """

  def key_pair(bits \\ 4096) do
    priv_dir = Application.fetch_env!(:honey_pot, :priv_dir)

    {:RSAPrivateKey, _, modulus, publicExponent, _, _, _, _exponent1, _, _, _otherPrimeInfos} =
      rsa_private_key = :public_key.generate_key({:rsa, bits, 65537})

    rsa_public_key = {:RSAPublicKey, modulus, publicExponent}
    pem_entry = :public_key.pem_entry_encode(:RSAPrivateKey, rsa_private_key)
    private_key = :public_key.pem_encode([pem_entry])
    public_key = :ssh_file.encode([{rsa_public_key, []}], :openssh_key)

    {:ok, pub} = File.open(priv_dir <> "/ssh_host_rsa_key.pub", [:write])
    {:ok, priv} = File.open(priv_dir <> "/ssh_host_rsa_key", [:write])
    :ok = IO.binwrite(pub, ~s/#{public_key}/)
    :ok = IO.binwrite(priv, ~s/#{private_key}/)
    :ok = File.close(pub)
    :ok = File.close(priv)
  end
end
