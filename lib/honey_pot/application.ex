defmodule HoneyPot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      HoneyPotWeb.Telemetry,
      HoneyPot.Repo,
      {DNSCluster, query: Application.get_env(:honey_pot, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: HoneyPot.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: HoneyPot.Finch},
      # Start a worker by calling: HoneyPot.Worker.start_link(arg)
      # {HoneyPot.Worker, arg},
      # Start to serve requests, typically the last entry
      HoneyPotWeb.Endpoint,
      HoneyPot.SSHDaemon
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: HoneyPot.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HoneyPotWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
