defmodule HoneyPot.Repo do
  use Ecto.Repo,
    otp_app: :honey_pot,
    adapter: Ecto.Adapters.Postgres
end
