# SPDX-License-Identifier: Apache-2.0
defmodule HoneyPot.SSHDaemon do
  @moduledoc """
  The module that encompasses the SSH socket acceptor and
  its dispatch.

  Taken from https://github.com/jbenden/esshd
  """

  use GenServer
  require Logger

  @type handler :: :elixir | :erlang | {module(), atom(), any()}

  @doc false
  def start_link(_args) do
    enabled = Application.fetch_env!(:esshd, :enabled)

    GenServer.start_link(
      __MODULE__,
      %{pid: nil, enabled: enabled},
      name: __MODULE__
    )
  end

  @doc false
  def init(%{enabled: true} = state) do
    # start listening for incoming SSH connections
    GenServer.cast(self(), :start)

    {:ok, state}
  end

  @doc false
  @spec init(map()) :: {:ok, map()}
  def init(%{enabled: false} = state) do
    # we're not enabled so return a dummy response
    {:ok, state}
  end

  @doc false
  @spec handle_info(map | any, map) :: {:noreply, map}
  def handle_info(_, state), do: {:noreply, state}

  @doc false
  def handle_cast(:start, state) do
    # Gather settings from the application configuration
    port = Application.fetch_env!(:esshd, :port)

    priv_dir =
      Application.fetch_env!(:esshd, :priv_dir)
      # credo:disable-for-next-line
      |> String.to_charlist()

    parallel_login = Application.fetch_env!(:esshd, :parallel_login)
    max_sessions = Application.fetch_env!(:esshd, :max_sessions)
    idle_time = Application.fetch_env!(:esshd, :idle_time)
    negotiation_timeout = Application.fetch_env!(:esshd, :negotiation_timeout)

    preferred_algorithms =
      Application.fetch_env!(:esshd, :preferred_algorithms) ||
        :ssh.default_algorithms()

    subsystems = Application.fetch_env!(:esshd, :subsystems)

    case :ssh.daemon(port,
           subsystems: subsystems,
           system_dir: priv_dir,
           user_dir: priv_dir,
           user_passwords: [],
           parallel_login: parallel_login,
           max_sessions: max_sessions,
           id_string: :random,
           idle_time: idle_time,
           negotiation_timeout: negotiation_timeout,
           preferred_algorithms: preferred_algorithms,
           failfun: &on_shell_unauthorized/3,
           connectfun: &on_shell_connect/3,
           disconnectfun: &on_shell_disconnect/1,
           pwdfun: &on_password/4
         ) do
      {:ok, pid} ->
        # link the created SSH daemon to ourself
        Process.link(pid)

        # Return, with state, and sleep
        {:noreply, %{state | pid: pid}, :hibernate}

      {:error, :eaddrinuse} ->
        :ok = Logger.error("Unable to bind to local TCP port; the address is already in use")
        # raise RuntimeError, "TCP port #{port} is in use"
        {:noreply, state, :hibernate}

      {:error, err} ->
        raise "Unhandled error encountered: #{inspect(err)}"
    end
  end

  @type ip_address :: :inet.ip_address()
  @type port_number :: :inet.port_number()
  @type peer_address :: {ip_address, port_number}

  @doc false
  @spec on_password(
          username :: charlist,
          password :: charlist,
          peer_address :: peer_address,
          state :: any
        ) :: :disconnect | {boolean, map}
  def on_password(username, password, peer_address, _state) do
    Logger.debug(fn ->
      "Checking #{inspect(username)} with password #{inspect(password)} from #{inspect(peer_address)}"
    end)

    logger(%{
      username: username,
      password: password,
      peer_address: peer_address
    })

    :disconnect
  end

  @doc false
  @spec on_shell_connect(String.t(), peer_address, String.t()) :: any
  def on_shell_connect(_username, _peer_address, _method) do
    :do_nothing
  end

  @doc false
  @spec on_shell_unauthorized(String.t(), peer_address, term) :: any
  def on_shell_unauthorized(username, {ip, port}, reason) do
    Logger.warning("""
    Authentication failure for #{inspect(username)} from #{inspect(ip)}:#{inspect(port)}: #{inspect(reason)}
    """)
  end

  @doc false
  @spec on_shell_disconnect(any) :: :ok
  def on_shell_disconnect(_) do
    :ok
  end

  defp logger(log) do
    @log_module.log(log)
  end
end
